
- This website is built automatically by gitlab every time you edit the files in the docs folder
- It does so thanks to [Mkdocs](https://mkdocs.org) a static site generator written in Python
- You must start customizing the file mkdocs.yml with your information
- You can change the looks of your website using mkdocs themes, you can find in the mkdocs.yml the names of the available themes

