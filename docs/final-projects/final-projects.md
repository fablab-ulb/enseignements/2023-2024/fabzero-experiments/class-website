# Final Projects

## Assignment

By groups, you will design, fabricate and document a proof of concept of a scientifically-based and frugal solution to solve a real-world problem you have identified.

* You will position your project along the [17 Sustainable Development Goals](https://sdgs.un.org/) listed by the United Nations.
* You will show scientific evidence with sources and references on which you build on your project.
* You will use at least one digital fabrication technique that is used in a fablab.
* You will explain the team dynamics and demonstrate how you worked as a team.

## Preparation for the final presentation

For the final presentation, you will prepare

* a graphical abstract illustrating your project (1920x1080px PNG image entitled "firstname.surname-slide.png", examples [here](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/class-website/final-projects/final-projects/) and [here](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/class-website/final-projects/final-projects/)) with :
    * a photo/image illustrating your project and prototype
    * [a logo of the FabLab ULB](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/class/-/tree/main/vade-mecum/images)
    * a short and clear question that your project tries to answer
* a pdf presentation (16 slides/8min) to share your project with an interdisciplinary audience ("firstname.surname-presentation.pdf").
* prototypes, proof of concept and technical/scientific documents that illustrate your process and your final achievement.
* a printed QR code linking to your online and finalized documentation


## Final projects documentation

| | |
| --- | --- |
|![Group 1 - Champi-non](img/group-01.png) |**Group 1 - Champi-non** <br> * [project docs](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/groups/group-01/) <br> * [final pres.](img/group-01.pdf) |
|![Group 2 - Earplugs](img/group-02.png) |**Group 2 - Earplugs** <br> * [project docs](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/groups/group-02/) <br> * [final pres.](img/group-02.pdf) |
|![Group 3 - Filtrao](img/group-03.png) |**Group 3 - Filtrao** <br> * [project docs](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/groups/group-03/) <br> * [final pres.](img/group-03.pdf) |
|![Group 4 - A.L.E.D.](img/group-04.png) |**Group 4 - A.L.E.D.** <br> * [project docs](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/groups/group-04/) <br> * [final pres.](img/group-04.pdf) |
|![Group 5 - alt Téflon](img/group-05.png) |**Group 5 - alt Téflon** <br> * [project docs](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/groups/group-05/) <br> * [final pres.](img/group-05.pdf) |
|![Group 6 -  REFABRIC](img/group-06.png) |**Group 6 - REFABRIC** <br> * [project docs](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/groups/group-06/) <br> * [final pres.](img/group-06.pdf) |
|![Group 7 - Liban Light](img/group-07.png) |**Group 7 - Liban Light** <br> * [project docs](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/groups/group-07/) <br> * [final pres.](img/group-07.pdf) |
|![Group 8 - Droit au logement](img/group-08.png) |**Group 8 - Droit au logement** <br> * [project docs](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/groups/group-08/) <br> * [final pres.](img/group-08.pdf) |
|![Group 9 - Antitremblement](img/group-09.png) |**Group 9 - Antitremblement** <br> * [project docs](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/groups/group-09/) <br> * [final pres.](img/group-09.pdf) |
