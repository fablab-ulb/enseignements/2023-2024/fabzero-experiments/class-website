# 1. Project management and documentation

## Goal of this unit

The goal of this unit is to learn

- some easy but powerful tools to write better documentation and share it using version control systems.
- some tips for successful Project Management

## Class materials

### Tutorials :

- [Command Line Interface CLI](https://gitlab.com/fablab-ulb/enseignements/fabzero/basics/-/blob/main/command-line.md)
- [Documentation and version control](https://gitlab.com/fablab-ulb/enseignements/fabzero/basics/-/blob/main/documentation.md)
- [Project management principles](https://gitlab.com/fablab-ulb/enseignements/fabzero/basics/-/blob/main/project-management-principles.md)

### Mind map

<embed src="../docs-tube.pdf?zoom=100" width="100%" height="500px">
</embed>

### Quick links

* [Access to the FabZero-Experiments class space on GitLab](https://gitlab.com/fablab-ulb/enseignements/2023-2024/fabzero-experiments)

### Class podcasts

* 2022-2023 - Podcast: Project management and documentation
    * [Project management and documentation (part 1)](https://ezplayer.ulb.ac.be/ezplayer/index.php?action=view_asset_details&album=PHYS-F-517-pub&asset=2023_02_16_10h29&asset_token=HAAQFLHM)
    * [Project management (part 2) [12min10s - 19min30s]](https://ezplayer.ulb.ac.be/ezplayer/?action=view_album_assets&album=PHYS-F-517-pub&token=HTEHSMVM)

## Assignment

* work through a git tutorial
* use git to build a personal site in the class archive describing you.
* test and apply project management principles all along the class.
* document your learning path for this module.
* Evaluate your work ([template](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/template/-/issues))