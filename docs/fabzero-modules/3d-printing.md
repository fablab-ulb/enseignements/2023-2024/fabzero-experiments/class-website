# 3D printing

## Goal of this unit

In this unit, you will learn to

* Identify the advantages and limitations of 3D printing.
* Apply design methods and production processes using 3D printing.

## Before class

* Install the [slicing software](https://en.wikipedia.org/wiki/Slicer_(3D_printing)) : [PrusaSlicer](https://help.prusa3d.com/en/article/install-prusaslicer_1903) on your computer.
* Follow [this 3D printing tutorial](https://gitlab.com/fablab-ulb/enseignements/fabzero/3d-print/-/blob/main/3D-printers.md) and prepare the gcode, using the slicing softwate, to print the 3D part you designed last week. Adapt your design so the printing time of your part does not exceed 20 to 30 minutes.

## Class materials

* 3D printing
   * [torture test](https://www.thingiverse.com/thing:2806295)  
   * [Design rules]( https://help.prusa3d.com/en/article/modeling-with-3d-printing-in-mind_164135)  
   * [Materials](https://blog.prusaprinters.org/advanced-filament-guide_39718/)  
   * [CNC Kitchen YouTube channel](https://www.youtube.com/@CNCKitchen)

## Examples from FabZero

* [Characteriez and optimize the design parameters](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/victor.depillecyn/fabzero-modules/module03/)
* [Make a flexible kit and machine in groups](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/gilles.martin/fabzero-modules/module03/)
* Some other machines : [Virginie Louckx](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/virginie.louckx/fabzero-modules/module03/), [Pauline Mackelbert](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/pauline.mackelbert/FabZero-Modules/module04/)

## Assignment

* In group, test the design rules, comment on the choice of materials and good practices for your 3D printer(s) regarding the [kit parts you designed last week](/fabzero-modules/computer-aided-design/) that you are going to make.
* In group, test and set the parameters of your flexible part so they can be combined with other classmate parts.
* Individually, 3D print your flexible construction kit you made.
* In group, make a compliant mechanism out of it with several hinges that do something.
