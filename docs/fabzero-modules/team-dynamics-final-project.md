# 5. Team dynamics and final projet

## 5.1 Project Analysis and Design (26/9/23 class)

### Class materials

- Problem and objective trees analysis ([video](https://m.youtube.com/watch?v=9KIlK61RInY))  
    - Create a problem tree
        - step 1 : identify the main **problem** (trunk)
        - step 2: identify the primary problem **causes** (roots) and the causes behind the causes (deeper roots)
        - step 3 : identify the problem direct **consequences** (branches) and secondary effects  (ramifications)
    - Transform your problem tree into an objective tree 
        - step 1 : rephrase the problem into an **objective**
        - step 2 : replace the causes by the **activities** to develop to reach the objective
        - step 3 : rephrase the problem consequences into project **outcomes**.

### Assignment :

Individually or in pairs of support, 

- Take inspiration from [fablab projects](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/class-website/#archives) and [frugal science projects](https://gitlab.com/fablab-ulb/enseignements/fabzero/projects/-/blob/main/open-source-lab.md) presented in class this week and choose a project that you care about.
- Analyze the project and construct a problem tree and an objective tree. Post the diagrams (ideally in vectorial images SVG) on module 5 of your website and describe it briefly.

## 5.2 Group formation and brainstorming on societal issues (24/10/23 class)

### Before class

- Find and bring in class an object that represents a theme or a societal issue that you care about.

### Assignment

Individually,

- Introspect and describe your experience (and feelings) about the process you went through to move from your object to a group, as well as a series of issues in the form of questions related to possible projects. There is no right or wrong here !
- Describe the object you've chosen and list the series of problems in the form of questions you've identified as a group. 

## 5.3 Group dynamics (6-7/11/23 class)

### Goal of this unit

In this unit, you will learn to use tools to function better in a group. Specifically, you will learn how to structure your meetings, define and assign roles, use decision-making techniques, communicate with each other, give everyone a voice and provide feedback. 

### Class materials

- [Slides of the Group Dynamics presentation](./../files/Fabzero-Exp-Group-Dynamics-presentation.pdf)


### Assignment

Individually (to report on your module 5 individual website page),

- Develop and document 3 tools for group dynamics you have learned and explain and how you could use them in the future.

In project group (to report on the group gitlab page that is going to be set up soon),

- Explain how your group works.
    - What do you already have in place?
    - How will you, as a group, structure your meeting? 
    - What roles have you identified for meetings and projects in general? 
    - How will you make decisions within your group? 
    - How will you communicate within your group?

## 5.4 Problem and objective of your group project (6-7/11/23 class)

### Goal of this unit

Through the constuction of problem trees, choose, define and analyse problems you would like as a group to work on. 

### Assignment

In project group,

- based on your previous group brainstorming, build one or multiple problem trees on problems you care about.
- using decision-making tools you learned, choose a problem you would like to work on as a group.
- describe the problem and answer the following questions regarding the problem :
    - What is the main problem ?
    - What are the facts ? Cite reliable sources.
    - What are the problems behind the problem ?
    - What would be missing to improve the situation ?
    - What are the ressources you have to move forward ?
    - What are the next steps ?
- Construct a problem tree and post it on your group documentation for Sunday 12/11/23 at midnight.
- Document your steps on your group project page. 

## 5.5 *24h Hackathon* - Prototyping a solution (13-14/11/23 classes)

### Goal of this unit

Starting from your problem trees, you will design an objective tree that should help you to identify, prototype and present a possible solution in a very short time. It will force you to make decisions, be creative and identify the strengths and weaknesses of your project.

### Before class

Take something to work with (paper, cardboard, scissors, markers, sticky paper, string...).

### Assignment

In project group, on your group project page and after these classes,

- briefly document the problematic you chose to work on.
- briefly document the solution prototypes you made and how it is answering the problematic
- post a hero shot of your last prototype.
- reflect as a group on the strengths, weaknesses/risks, and challenges you identified in order to successfully complete your project. What are the next steps ?


