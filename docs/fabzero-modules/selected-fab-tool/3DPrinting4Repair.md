## 3DP4R - 3D Printing For Repair

## Goal of this unit

In this unit, you will learn
- The basics of repair for electrical appliances
- the guidelines for creating a 3D printable version of a spare part needed to repair a product

## Before class

- Read the [*3D printing for Repair* Guide](https://textbooks.open.tudelft.nl/textbooks/catalog/book/49) (also available in [french](https://drive.google.com/file/d/1xN7_6a7Tg_olNLeVP0OxKJDjMBM2HjtN/view?usp=share_link)) 
- Find and bring in class a broken (portable) object that you will repair.
- the object should have a broken plastic part that you think is 3D printable (according to the 3D printing for Repair Guide)
- the plastic part should not be available on a design sharing platform like [Thingiverse](https://www.thingiverse.com) or [Instructables](https://www.instructables.com/). Check it ! (otherwise it’s too easy ;-) )

## Class materials

- [Tutorial on the Basics of Repair](https://drive.google.com/file/d/1WNELj4k4HLaBgcLn-rATlArg9Qt--ZHe/view)
- ["3D printing for Repair" Guide](https://textbooks.open.tudelft.nl/textbooks/catalog/book/49)

## Assignment

- Disassemble your object to access the broken part and its related parts
- Analyse the broken part of your object to define if it is suitable for 3D printing or not
- (re)Design the part, using a CAD software, to fit the requirements and constraints of its new manufacturing method (3D printing) 
- Manufacture your part and post-process it 
- Test your part to evaluate its quality and performance
- Put it back in the device, make it work and be proud !
- Document your process with text and images on your website and include your original design files with an appropriate license.