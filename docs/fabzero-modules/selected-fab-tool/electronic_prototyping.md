# Electronic Prototyping

## Goal of this unit

In this unit, you will learn to use a microcontroller development board to prototype electronic applications by programming it and using input and output devices.

## Before class

Install the [Arduino IDE](https://www.arduino.cc/en/software) on your computer.

## Class materials

- [Guide on how to program your RP2040 microcontroller](https://fablab-ulb.gitlab.io/enseignements/fabzero/electronics/)

Examples and libraries

- [MicroPython library](https://github.com/flrrth/pico-dht20)
- [Arduino library](https://github.com/flrrth/pico-dht20)
- [C examples for I2C devices](https://github.com/raspberrypi/pico-examples/tree/master/i2c)

Get inspired:

- [Projet CO2 - Mesurer le CO2 pour mieux aérer](http://projetco2.fr/)

## Assignment

DHT20 - Temperature & RH sensor

- Read the [sensor's datasheet](https://cdn.sparkfun.com/assets/8/a/1/5/0/DHT20.pdf) and identify relevant characteristics
- Connect the sensor to your development board
- Write a program to read data and send it to your computer over usb
- Write a program to process data and display some information using the onboard LED

Challenge 

- Make your board do something : add an output device to a development board and program it to do something
