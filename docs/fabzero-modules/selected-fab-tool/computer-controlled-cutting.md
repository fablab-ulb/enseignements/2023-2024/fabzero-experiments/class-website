# Computer Controlled Cutting

## Goal of this unit

In this unit, you will learn how to design, prepare 2D CAD files and lasercut them.

## Before class

* Install a 2D vector imagery tool, [Inkscape](https://inkscape.org/), and follow some [Inkscape basic tutorials](https://inkscape.org/learn/tutorials/)
* Read and study [this laser cutter manual](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut). You will be quizzed at the beginning of the class (particularly in the area of security) !


## Class materials

* [Laser cutter manual](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut)

## Assignment

Group assignment:

* characterize your lasercutter's focus, power, speed, kerf and joint clearance

Individual assignment:

* design, lasercut, and document an original construction kit, accounting for the lasercutter kerf, which can be assembled in multiple ways.

## Learning outcome

* Demonstrate and describe 2D design processes
* Identify and explain processes involved in using the laser cutter.
* Develop, evaluate and construct the 3D object made from your contruction kit
